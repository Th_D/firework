#include "fusee.h"

Fusee::Fusee(Form** tf, short unsigned int* nbf, Color color, double tailleProj, double dureeVieProjectiles){
  //std::cout<<"const Fusee DEFAULT"<<std::endl;
  tabform = tf;
  nbform = nbf;
  dureeDeVie = 0.5;
  getAnim().setSpeed(Vector(1,5,1));
  getAnim().setAccel(Vector(0,G,0));
  this->tailleProj = tailleProj;
  enVie=true;
  this->dureeVieProjectiles = dureeVieProjectiles;
}
//1280/1024
Fusee::Fusee(Form** tf, short unsigned int* nbf, double dureeDeVie, int nbProjectiles, Color color, double tailleProj, Vector v0, double dureeVieProjectiles, double vP){
  this->color = color;
  this->nbProjectiles = nbProjectiles;
  this->dureeDeVie = dureeDeVie;
  this->tailleProj = tailleProj;
  this->vP = vP;
  getAnim().setAccel(Vector(0,G,0));
  //std::cout<<"const Fusee DEFAULT"<<std::endl;
  tabform = tf;
  enVie=true;
  nbform = nbf;
  getAnim().setSpeed(v0);//Vector(1,5,1)
  this->dureeVieProjectiles = dureeVieProjectiles;
}

void Fusee::addTempsDeVie(double dt){
    tempsDeVie+=dt;
    //std::cout<<"\t\ttempsDeVie = "<<tempsDeVie<<std::endl;
    enVie = (tempsDeVie < dureeDeVie);
    if (tempsDeVie<0)
        enVie = false;
    /*if (enVie)
        std::cout<<"vivante"<<std::endl;
    else
        std::cout<<"morte"<<std::endl;*/
}

bool Fusee::isAlive(){
    return this->enVie;
}

void Fusee::update(double delta_t)
{
    addTempsDeVie(delta_t);
    //std::cout<<"\tupdate Fusee"<<std::endl;
    Point org = anim.getPos();
    //std::cout<<org.y<<std::endl;
    anim.setSpeed(update_vit(anim.getSpeed(), anim.getAccel(), delta_t));
    org.y = org.y + (delta_t*anim.getSpeed().y);
    org.x = org.x + (delta_t*anim.getSpeed().x);
    org.z = org.z + (delta_t*anim.getSpeed().z);


    //std::cout<<delta_t<<std::endl;
    if (this->isAlive()){
        anim.setPos(org);
    }
    if (!this->isAlive() && !aExplose){ //EXPLOSION
        anim.setPos(org);
        Vector vProjectiles[MAXB];
        int spdProjectiles = this->vP;
        v_boulettes(nbProjectiles,spdProjectiles, anim.getSpeed(), vProjectiles);
        for (int i=0; i<nbProjectiles; i++){
            Color col = this->color;
            if(col.r==-1 && col.g==-1 && col.b==-1){
                col = Color(static_cast <float> (rand())/static_cast <float> (RAND_MAX),static_cast <float> (rand())/static_cast <float> (RAND_MAX),static_cast <float> (rand())/static_cast <float> (RAND_MAX));
            }
            Particule *part = new Particule(Point(org.x+ (delta_t*4*anim.getSpeed().x), org.y+8.5+ (delta_t*4*anim.getSpeed().y), org.z+ (delta_t*4*anim.getSpeed().z)), vProjectiles[i], col, tailleProj, dureeVieProjectiles);
            std::cout<<org.x<<" | "<< org.y<<" | "<<org.z<<" | "<<std::endl;
            //tabform[i] = part;
            //nbform++;
            projectiles[i] = part;
            nbP++;
        }
        aExplose = true;
    }
    if (nbP>0){
        if (!projectiles[0]->isAlive()){
            for (int i=0; i<nbP; i++){
                delete projectiles[i];
                //std::cout<<"DESTR"<<std::endl;
            }
            nbP = 0;
        }

        for (int i=0; i<nbP; i++)
            projectiles[i]->update(delta_t);
    }
}

void Fusee::render()
{
    //std::cout<<"\trender Fusee!!!!!!!!!!!!!!!!!!!"<<nbP<<std::endl;
    if(enVie){ //affichage de la fusee
        GLUquadric* quad = gluNewQuadric(); //cr�ation du quadrique
        glScaled(0.3,0.3,0.3);
        Form::render();
        glRotated(-90, 1,0,0);
         gluCylinder(quad,0.2,0,0.35,20,20); //c�ne 1
        glTranslated(0,0,-0.20); //je descends pour faire le 2�me c�ne
        gluCylinder(quad,0.1,0.1,0.2,20,20); //c�ne 2
        gluDeleteQuadric(quad); //je supprime le quadrique
    }
    //affichage des projectiles
    if (nbP>0){
            //std::cout<<projectiles[0]->tempsDeVie<<std::endl;;
        if (!projectiles[0]->isAlive())
            nbP = 0;
        for (int i=0; i<nbP; i++){
            //std::cout<<"\trender part from fusee!!!!!!!!!!!!!!!!!!!"<<i<<std::endl;
            glPushMatrix(); // Preserve the camera viewing point for further forms
            projectiles[i]->render();
            glPopMatrix();
        }
         if (!projectiles[0]->isAlive()){
            for (int i=0; i<nbP; i++)
                delete projectiles[i];
            nbP = 0;
        }
    }
}


