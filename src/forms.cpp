#include <cmath>
#include <SDL2/SDL_opengl.h>
#include <GL/GLU.h>
#include "forms.h"


using namespace std;


void Form::update(double delta_t)
{
    // Nothing to do here, animation update is done in child class method
}


void Form::render()
{
    // Point of view for rendering
    // Common for all Forms
    Point org = anim.getPos();
    glTranslated(org.x, org.y, org.z);
    glColor3f(col.r, col.g, col.b);
}


Sphere::Sphere(double r, Color cl)
{
    radius = r;
    col = cl;
}


void Sphere::update(double delta_t)
{
    // Complete this part
    Point org = anim.getPos();
    org.y = org.y + 0.02*anim.getSpeed().y;
    anim.setPos(org);

}


void Sphere::render()
{
    GLUquadric *quad;

    quad = gluNewQuadric();

    // Complete this part
    glScaled(0.1,0.1,0.1);
    Form::render();
    glBegin(GL_QUADS);
    {
        glTranslated(3,0,1);
        gluSphere(quad,1,5,5);
    }
    glEnd();

    gluDeleteQuadric(quad);

}


Cube_face::Cube_face(Vector v1, Vector v2, Point org, int im, double l, double w, Color cl)
{
    vdir1 = 1.0 / v1.norm() * v1;
    vdir2 = 1.0 / v2.norm() * v2;
    anim.setPos(org);
    length = l;
    width = w;
    col = cl;
    image = im;


    //glEnable(GL_DEPTH_TEST);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

    /*
    texture = loadTexture("herbe.png");
    glBindTexture(GL_TEXTURE_2D, texture);
    */

    if (image == 1){
        loadTextureCube("herbesombre.jpg");
    }
    if (image == 2){
        loadTextureCube("villenoir.jpg");
    }
    if (image == 3){
        loadTextureCube("villenoir2.jpg");
    }

}


void Cube_face::update(double delta_t)
{
    // Do nothing, no physics associated to a Cube_face
}

void Cube_face::loadTextureCube(char* fileName)
{
    texture = loadTexture(fileName);
    //glBindTexture(GL_TEXTURE_2D, texture);

}

void Cube_face::render()
{
    Point p1 = Point();
    Point p2 = p1, p3, p4 = p1;
    p2.translate(length*vdir1);
    p3 = p2;
    p3.translate(width*vdir2);
    p4.translate(width*vdir2);

    Form::render();
    if (texture == NULL) {
        glBegin(GL_QUADS);
        {
            //glColor3f(1,1,0);

            glVertex3d(p1.x, p1.y, p1.z);

           // glColor3f(0,1,1);
            glVertex3d(p2.x, p2.y, p2.z);

          //  glColor3f(1,0,1);
            glVertex3d(p3.x, p3.y, p3.z);

          // glColor3f(0,1,0);
            glVertex3d(p4.x, p4.y, p4.z);

        }
        glEnd();
    }
    else {
            glBindTexture(GL_TEXTURE_2D, texture);
            glEnable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
        {

            //glColor3f(1,1,0);
            glTexCoord2d(0,1);
            glVertex3d(p1.x, p1.y, p1.z);
            glTexCoord2d(0,0);
           // glColor3f(0,1,1);
            glVertex3d(p2.x, p2.y, p2.z);
            glTexCoord2d(1,0);
          //  glColor3f(1,0,1);
            glVertex3d(p3.x, p3.y, p3.z);
            glTexCoord2d(1,1);
          // glColor3f(0,1,0);
            glVertex3d(p4.x, p4.y, p4.z);

        }
        glEnd();
        glDisable(GL_TEXTURE_2D);

    }
    //glDisable(GL_TEXTURE_2D);
}

Requette::Requette(Vector v1, Vector v2, Point org, double l, double w)
{
    vdir1 = 1.0 / v1.norm() * v1;
    vdir2 = 1.0 / v2.norm() * v2;
    anim.setPos(org);
    length = l;
    width = w;

}

void Requette::update(double delta_t)
{
    // Complete this part
    Point org = anim.getPos();
    org.y = org.y + 0.02*anim.getSpeed().y;
    anim.setPos(org);

}

void Requette::render()
{


    GLUquadric* quad = gluNewQuadric(); //cr�ation du quadrique

    glScaled(0.3,0.3,0.3);

    Form::render();

    glRotated(-90, 1,0,0);

    gluCylinder(quad,0.3,0,0.8,20,20); //c�ne 1

    glTranslated(0,0,-1.05); //je descends pour faire le 2�me c�ne

    gluCylinder(quad,0.3,0.3,1,20,20); //c�ne 2

    gluDeleteQuadric(quad); //je supprime le quadrique




}
