// Using SDL, SDL OpenGL and standard IO
#include <iostream>
#include <cmath>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <GL/GLU.h>
#include <AntTweakBar.h>

// Module for space geometry
#include "geometry.h"
// Module for generating and rendering forms
#include "forms.h"
#include "fusee.h"
#include "particule.h"
#include "Utilitaire.h"

using namespace std;


/***************************************************************************/
/* Constants and functions declarations                                    */
/***************************************************************************/
// Screen dimension constants
const int SCREEN_WIDTH = 1400;
const int SCREEN_HEIGHT = 1000;

//Variables du menu
unsigned char couleurProjectile[] = { 2, 0, 0 }; // Model color (32bits RGBA)
double dureeVieProjectile = 1.5;
double dureeVieFusee = 1.5;
//double speed = 3.0;
double vitesseProjectile = 7.;
int nboulettes = 30;
double speedX = 0;
double speedY = 20;
double speedZ = 0;

SDL_Event event;
int handled;

int x = 0;
int y = 0;

// Max number of forms : static allocation
const int MAX_FORMS_NUMBER = 1000;

// Animation actualization delay (in ms) => 100 updates per second
const Uint32 ANIM_DELAY = 10;

int angleX = 0;
int angleY = 0;
double valZ = 0;

// Starts up SDL, creates window, and initializes OpenGL
bool init(SDL_Window** window, SDL_GLContext* context);

// Initializes matrices and clear color
bool initGL();

// Updating forms for animation
void update(Form* formlist[MAX_FORMS_NUMBER], double delta_t);

// Renders scene to the screen
const void render(Form* formlist[MAX_FORMS_NUMBER], const Point &cam_pos);

// Frees media and shuts down SDL
void close(SDL_Window** window);

double facteurT = 1;

/***************************************************************************/
/* Functions implementations                                               */
/***************************************************************************/
bool init(SDL_Window** window, SDL_GLContext* context)
{
    // Initialization flag
    bool success = true;

    // Initialize SDL
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << endl;
        success = false;
    }
    else
    {
        // Use OpenGL 2.1
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

        // Create window
        *window = SDL_CreateWindow( "TP intro OpenGL / SDL 2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
        if( *window == NULL )
        {
            cout << "Window could not be created! SDL Error: " << SDL_GetError() << endl;
            success = false;
        }
        else
        {
            // Create context
            *context = SDL_GL_CreateContext(*window);
            if( *context == NULL )
            {
                cout << "OpenGL context could not be created! SDL Error: " << SDL_GetError() << endl;
                success = false;
            }
            else
            {
                // Use Vsync
                if(SDL_GL_SetSwapInterval(1) < 0)
                {
                    cout << "Warning: Unable to set VSync! SDL Error: " << SDL_GetError() << endl;
                }

                // Initialize OpenGL
                if( !initGL() )
                {
                    cout << "Unable to initialize OpenGL!"  << endl;
                    success = false;
                }
            }
        }
    }

    return success;
}


bool initGL()
{
    bool success = true;
    GLenum error = GL_NO_ERROR;

    // Initialize Projection Matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Set the viewport : use all the window to display the rendered scene
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    // Fix aspect ratio and depth clipping planes
    gluPerspective(40.0, (GLdouble)SCREEN_WIDTH/SCREEN_HEIGHT, 1.0, 100.0);


    // Initialize Modelview Matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Initialize clear color : black with no transparency
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f );

    // Activate Z-Buffer


    // Check for error
    error = glGetError();
    if( error != GL_NO_ERROR )
    {
        cout << "Error initializing OpenGL!  " << gluErrorString( error ) << endl;
        success = false;
    }

    glEnable(GL_DEPTH_TEST);

    return success;
}

void update(Form* formlist[MAX_FORMS_NUMBER], double delta_t)
{
    // Update the list of forms
    unsigned short i = 0;
    while(formlist[i] != NULL)
    {
        formlist[i]->update(delta_t);
        i++;
    }
}

const void render(Form* formlist[MAX_FORMS_NUMBER], const Point &cam_pos, const Point &cib_pos,const Point &cam_rota )
{
    // Clear color buffer and Z-Buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Initialize Modelview Matrix
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    // Set the camera position and parameters
    gluLookAt(cam_pos.x,cam_pos.y,cam_pos.z, cib_pos.x,cib_pos.y,cib_pos.z, cam_rota.x,cam_rota.y,cam_rota.z);
    glTranslated(0,0,valZ);

    glTranslated(0,0,-3);
    glRotated(5,1,0,0);

    glRotated(angleY,0,1,0);
    glRotated(angleX,1,0,0);
    // Isometric view
    /*
    glRotated(-45, 0, 1, 0);
    glRotated(30, 1, 0, -1);

    */
    // X, Y and Z axis
    glPushMatrix(); // Preserve the camera viewing point for further forms
    // Render the coordinates system

    glBegin(GL_LINES);
    {
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3i(0, 0, 0);
        glVertex3i(1, 0, 0);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3i(0, 0, 0);
        glVertex3i(0, 1, 0);
        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3i(0, 0, 0);
        glVertex3i(0, 0, 1);
    }
    glEnd();
    glPopMatrix(); // Restore the camera viewing point for next object

    // Render the list of forms
    unsigned short i = 0;
    while(formlist[i] != NULL)
    {
        //if(formlist[i]->isAlive()){
            glPushMatrix(); // Preserve the camera viewing point for further forms
            //glScaled(0.5,0.5,0.5);
            //std::cout<<"\t\trender from FP"<<std::endl;
            formlist[i]->render();
            glPopMatrix(); // Restore the camera viewing point for next object
            i++;
        //}
    }
}

void close(SDL_Window** window)
{
    //Destroy window
    SDL_DestroyWindow(*window);
    *window = NULL;

    //Quit SDL subsystems
    SDL_Quit();
}


/***************************************************************************/
/* MAIN Function                                                           */
/***************************************************************************/
int main(int argc, char* args[])
{
    // The window we'll be rendering to
    SDL_Window* gWindow = NULL;

    // OpenGL context
    SDL_GLContext gContext;


    // Start up SDL and create window
    if( !init(&gWindow, &gContext))
    {
        cout << "Failed to initialize!" << endl;
    }
    else
    {
         //Menu
        TwInit(TW_OPENGL, NULL);
        TwWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);

        TwBar *myBar;
        myBar = TwNewBar("Parametres");

        //MENU : Changer la variable &speed avec le vecteur vitesse

        TwAddVarRW(myBar, "Vx", TW_TYPE_DOUBLE, &speedX, \
               " label='Vx (U/J)' min=0 max=30 step=0.1 help='Entrez votre vecteur vitesse pour définir la trajectoire'"); // A changer ?
        TwAddVarRW(myBar, "Vy", TW_TYPE_DOUBLE, &speedY, \
               " label='Vy (Y/H)' min=0 max=30 step=0.1"); // A changer ?
        TwAddVarRW(myBar, "Vz", TW_TYPE_DOUBLE, &speedZ,
               " label='Vz (T/G)' min=0 max=30 step=0.1"); // A changer ?
        TwAddVarRW(myBar, "nbP", TW_TYPE_INT32, &nboulettes,
               " label='NbP (I/K)' min=0 max=30 step=1");
        TwAddVarRW(myBar, "DVieP", TW_TYPE_DOUBLE, &dureeVieProjectile,
               " label='DVieP (C/V)' min=0 max=5 step=0.1");
        TwAddVarRW(myBar, "DVieF", TW_TYPE_DOUBLE, &dureeVieFusee,
               " label='DVieF (W/X)' min=0 max=5 step=0.1");
        TwAddVarRW(myBar, "VP", TW_TYPE_DOUBLE, &vitesseProjectile,
               " label='VP m/s (B/N)' min=0 max=5 step=0.1");
        TwAddVarRW(myBar, "deltaT", TW_TYPE_DOUBLE, &facteurT,
               " label='Temps (O/L)'");


        // Main loop flag
        bool quit = false;
        Uint32 current_time, previous_time, elapsed_time;
        // Event handler
        SDL_Event event;

        // Camera position
        Point camera_position(0, 0.0, 5.0);
        Point cible_position(0, 0, 0);
        Point camera_rotation(0, 1.0, 0.0);

        // The forms to render
        Form* forms_list[MAX_FORMS_NUMBER];
        unsigned short number_of_forms = 0, i;
        for (i=0; i<MAX_FORMS_NUMBER; i++)
        {
            forms_list[i] = NULL;
        }
        // Create here specific forms and add them to the list...
        // Don't forget to update the actual number_of_forms !
        Cube_face *pfirst_face = NULL;
        Sphere *pfirst_sphere1 = NULL;
        Sphere *pfirst_sphere2 = NULL;
        Requette *pfirst_requette = NULL;
        Fusee *pfirst_fusee = NULL;


        //pfirst_face = new Cube_face(Vector(1,0,0), Vector(0,0,1), Point(-0.5, -0.5, -0.5), 1,20,20);

        //sol
        pfirst_face = new Cube_face(Vector(1,0,0), Vector(0,0,1), Point(-25, -0.5, -25), 1,40,40);
        forms_list[number_of_forms] = pfirst_face;
        number_of_forms++;

        pfirst_face = new Cube_face(Vector(1,0,0), Vector(0,1,0), Point(-25, -7, -25), 2, 40, 40);
        forms_list[number_of_forms] = pfirst_face;
        number_of_forms++;


        pfirst_face = new Cube_face(Vector(0,0,1), Vector(0,1,0), Point(-25, -7, -25), 2, 40, 40);
        forms_list[number_of_forms] = pfirst_face;
        number_of_forms++;



        pfirst_face = new Cube_face(Vector(1,0,0), Vector(0,1,0), Point(-25, -7, 15),2,40,40);
        forms_list[number_of_forms] = pfirst_face;
        number_of_forms++;

/*

        pfirst_face = new Cube_face(Vector(1,0,0), Vector(0,1,0), Point(-0.5, -0.5, 0.5),2,40,40);
        forms_list[number_of_forms] = pfirst_face;
        number_of_forms++;*/


        pfirst_face = new Cube_face(Vector(0,1,0), Vector(0,0,1), Point(15, -7, -25),3,40,40);
        forms_list[number_of_forms] = pfirst_face;
        number_of_forms++;

       /* pfirst_face = new Cube_face(Vector(0,1,0), Vector(0,0,1), Point(15, -7, -25),3,40,40);
        forms_list[number_of_forms] = pfirst_face;
        number_of_forms++;*/


/*

        pfirst_sphere1 = new Sphere(5, Color(1,1,1));
        forms_list[number_of_forms] = pfirst_sphere1;
        number_of_forms++;

        pfirst_sphere1->getAnim().setSpeed(Vector(1,1,1));
        //pfirst_sphere1->update(1);


        pfirst_sphere2 = new Sphere(10, Color(0,1,0));
        forms_list[number_of_forms] = pfirst_sphere2;
        number_of_forms++;
*/
        /*
        pfirst_requette = new Requette(Vector(1,0,0), Vector(0,1,0), Point(-0.5, -0.5, -0.5));
        forms_list[number_of_forms] = pfirst_requette;
        number_of_forms++;
        pfirst_requette->getAnim().setSpeed(Vector(1,1,1));
        pfirst_requette->update(1);*/

        if (elapsed_time>1){ //pour eviter bug au lancement
            elapsed_time = 0;
        }
/*
        //Particule *pfirst_particule = new Particule(2,2,2);
        forms_list[number_of_forms] = pfirst_particule;
        number_of_forms++;
*/



        // Get first "current time"
        previous_time = SDL_GetTicks();
        // While application is running
        while(!quit)
        {
            // Handle events on queue
            while(SDL_PollEvent(&event) != 0)
            {
                handled = TwEventSDL(&event, SDL_MAJOR_VERSION, SDL_MINOR_VERSION);

                int x = 0, y = 0;
                SDL_Keycode key_pressed = event.key.keysym.sym;

                if( !handled ){
                    switch(event.type)
                {

                // User requests quit
                case SDL_QUIT:
                    quit = true;
                    break;
                case SDL_KEYDOWN:
                    // Handle key pressed with current mouse position
                    SDL_GetMouseState( &x, &y );

                    switch(key_pressed)
                    {
                    case SDLK_RIGHT:
                        angleY -=5;
                        break;
                    case SDLK_LEFT:
                        angleY +=5;
                        break;
                    case SDLK_UP:
                        angleX +=5;
                        break;
                    case SDLK_DOWN:
                        angleX -=5;
                        break;
                    case SDLK_LSHIFT:
                        valZ = 0;
                        angleX = 0;
                        //angleY = 0;
                        //camera_position.x = 8;
                        //camera_position.y = 3;
                        //camera_position.y = 4;
                        //camera_position.z = 13;
                        //camera_position.z = 27;
                        //camera_rotation.y = 1;
                        angleY = -45;
                        cible_position.y = 1.0;
                        camera_position.z += 4.6;
                        cible_position.z += 4.6;
                        break;
                    case SDLK_p:
                        camera_position.z -= 0.1;
                        cible_position.z -= 0.1;
                        break;
                    case SDLK_o:
                        facteurT += 0.1;
                        break;
                    case SDLK_l:
                        facteurT -= 0.1;
                        break;
                    case SDLK_u:
                        speedX += 0.3;
                        break;
                    case SDLK_j:
                        speedX -= 0.3;
                        break;
                    case SDLK_y:
                        speedY += 0.3;
                        break;
                    case SDLK_h:
                        speedY -= 0.3;
                    break;
                    case SDLK_t:
                        speedZ += 0.3;
                        break;
                    case SDLK_g:
                        speedZ -= 0.3; // dureeVieProjectile  dureeVieFusee
                        break;
                    case SDLK_m:
                        camera_position.z += 0.1;
                        cible_position.z += 0.1;
                        break;
                    case SDLK_c:
                        dureeVieProjectile -= 0.1;
                        break;
                    case SDLK_v:
                        dureeVieProjectile += 0.1;
                        break;
                    case SDLK_b:
                        vitesseProjectile -= 0.1;
                        break;
                    case SDLK_n:
                        vitesseProjectile += 0.1;
                        break;
                    case SDLK_w:
                        dureeVieFusee -= 0.1;
                        break;
                    case SDLK_x:
                        dureeVieFusee += 0.1;
                        break;
                    //deplacer point de vue
                    case SDLK_a:
                        cible_position.x -= 0.1;
                        break;
                    case SDLK_e:
                        cible_position.x += 0.1;
                        break;
                    case SDLK_z:
                        cible_position.y += 0.1;
                        break;
                    case SDLK_s:
                        cible_position.y -= 0.1;
                        break;
                    case SDLK_i:
                        nboulettes += 1;
                        break;
                    case SDLK_k:
                        nboulettes -= 1;
                        break;
                    // Quit the program when 'q' or Escape keys are pressed
                    case SDLK_SPACE:
                        std::cout<< "Fusee PERSO";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, dureeVieFusee, nboulettes, Color(1,0,0), 0.25, Vector(speedX,speedY,speedZ), dureeVieProjectile, vitesseProjectile); //vitesseProjectile
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_ESCAPE:
                        quit = true;
                        break;

                   case SDLK_1:
                        std::cout<< "Fusee 1";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 1,35, Color(1,0,0), 0.2, Vector(1,20,0), 1, 5);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_2:
                        std::cout<< "Fusee 2";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 2, 45, Color(0,0,1), 0.3, Vector(0,19,3), 0.7, 7);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_3:
                        std::cout<< "Fusee 3";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 1.5, 25, Color(-1,-1,-1),0.3, Vector(2,19,-3), 1.3, 9);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_4:
                        std::cout<< "Fusee 4";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 1.5, 14, Color(0.2,1,0.4),0.3, Vector(2,18,6), 1.3, 4);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_5:
                        std::cout<< "Fusee 5";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 1.5, 38, Color(0.6,0,0.8),0.2, Vector(-2,22,1), 1.3, 6);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_6:
                        std::cout<< "Fusee 6";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 1.5, 50, Color(-1,-1,-1),0.4, Vector(4,19,1), 1.3, 8);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_7:
                        std::cout<< "Fusee 7";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 1.5, 30, Color(-1,-1,-1),0.2, Vector(3,22,-3), 1.3, 3);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_8:
                        std::cout<< "Fusee 8";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 1.5, 15, Color(-1,-1,-1),0.3, Vector(-3,23,-3), 1.3, 6);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;
                    case SDLK_9:
                        std::cout<< "Fusee 9";
                        pfirst_fusee = new Fusee(forms_list, &number_of_forms, 1.5, 30, Color(-1,-1,-1),0.4, Vector(-7,23,2), 1.3, 5);
                        forms_list[number_of_forms] = pfirst_fusee;
                        number_of_forms++;
                        break;


                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
                }

            }

            // Update the scene
            current_time = SDL_GetTicks(); // get the elapsed time from SDL initialization (ms)
            elapsed_time = current_time - previous_time;
            if (elapsed_time > ANIM_DELAY)
            {
                previous_time = current_time;
                update(forms_list, 1e-3 * elapsed_time * facteurT); // International system units : seconds
            }

            // Render the scene
            render(forms_list, camera_position, cible_position, camera_rotation);

            // Draw the menu
            TwDraw();

            // Update window screen
            SDL_GL_SwapWindow(gWindow);
        }
    }

    // Free resources and close SDL
    close(&gWindow);

    return 0;
}
