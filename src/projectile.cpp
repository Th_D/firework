#include "projectile.h"


Projectile::Projectile(){
    //std::cout<<"const Projectile DEFAULT"<<std::endl;
}

Projectile::Projectile(double, double, double, double, double, double, double, double, double, double, double, double){
    //std::cout<<"const Projectile ARGS"<<std::endl;

}

Projectile::~Projectile(void){
    //std::cout<<"destructor of Projectile"<<std::endl;
}





void v_boulettes(int nboulettes, int norme_vitesse, Vector v, Vector tabRes[])
{
    int i,n;
    Vector v_temp;
    for (n = 0; n< nboulettes; n++){
        i = rand()%3;
        //std::cout<<i<<std::endl;
        switch(i){
        case 0 :
            v_temp.x = sqrt(rand()%norme_vitesse);
            v_temp.y = sqrt(rand()%int(norme_vitesse*norme_vitesse - v_temp.x*v_temp.x));
            v_temp.z = sqrt(-(v_temp.x*v_temp.x) - (v_temp.y*v_temp.y) + norme_vitesse*norme_vitesse);
            break;
        case 1 :
            v_temp.y = sqrt(rand()%norme_vitesse);
            v_temp.z = sqrt(rand()%int(norme_vitesse*norme_vitesse - v_temp.y*v_temp.y));
            v_temp.x = sqrt(-(v_temp.y*v_temp.y) - (v_temp.z*v_temp.z) + norme_vitesse*norme_vitesse);
            break;
        case 2 :
            v_temp.z = sqrt(rand()%norme_vitesse);
            v_temp.x = sqrt(rand()%int(norme_vitesse*norme_vitesse - v_temp.z*v_temp.z));
            v_temp.y = sqrt(-(v_temp.z*v_temp.z) - (v_temp.x*v_temp.x) + norme_vitesse*norme_vitesse);
            break;
        }
        int s = rand()%2;
        if (s){
            v_temp.z = -v_temp.z;
        }
        s = rand()%2;
        if (s){
            v_temp.y = -v_temp.y;
        }
        s = rand()%2;
        if (s){
            v_temp.x = -v_temp.x;
        }
        tabRes[n]=v_temp+v;
        //std::cout<<tabRes[n]<<std::endl;
    }
}
