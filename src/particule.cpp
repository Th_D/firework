#include "particule.h"


Particule::Particule(Point org, Vector vitesse, Color color, double taille, double dureeDeVie){
  //std::cout<<"const Particule DEFAULT"<<std::endl;
  getAnim().setPos(org);
  getAnim().setSpeed(vitesse);
  getAnim().setAccel(Vector(0,G,0));
  this->enVie = true;
  this->taille = taille;
  this->col = color;
  this->dureeDeVie = dureeDeVie;
}

Particule::Particule(Point org, Vector vitesse, Vector acc, Color color, double taille){
  //std::cout<<"const Particule DEFAULT"<<std::endl;
  dureeDeVie = 1.3 ;
  getAnim().setPos(org);
  getAnim().setSpeed(vitesse);
  getAnim().setAccel(acc);
  this->enVie=true;
  this->col = color;
  this->taille = taille;
}

Particule::~Particule(){
    //std::cout<<"\t\t\t\t\tdelete Particule"<<std::endl;
}

bool Particule::isAlive(){
    return (this->enVie);
}

void Particule::kill(){
    this->enVie = false;
}


void Particule::update(double delta_t)
{
    //std::cout<<"\tupdate Particule"<<std::endl;

    Point org = anim.getPos();
    anim.setSpeed(update_vit(anim.getSpeed(), anim.getAccel(), delta_t));
    Point nOrg = update_pos(Vector(anim.getPos().x, anim.getPos().y, anim.getPos().z), anim.getSpeed(), anim.getAccel(), delta_t);
    anim.setPos(nOrg);
    tempsDeVie+=delta_t;
    //std::cout<<tempsDeVie<<" < "<<dureeDeVie<<std::endl;
    if(tempsDeVie>dureeDeVie){
        enVie=false;
        //std::cout<<"particule detruite"<<std::endl;
    }
}

void Particule::render()
{
    //std::cout<<"\t\t\trender Particule"<<std::endl;
    if (this->enVie && this->tempsDeVie>=0){
        GLUquadric *quad;
        quad = gluNewQuadric();
        glScaled(0.2,0.2,0.2);
        Form::render();
        glColor3f(col.r, col.g, col.b);
        gluSphere(quad,this->taille,10,10);
        gluDeleteQuadric(quad);
    }
}
