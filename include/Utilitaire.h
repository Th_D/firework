#ifndef UTILITAIRE_H
#define UTILITAIRE_H


#include <GL/gl.h>
#include <SDL2/SDL.h>

#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 0x812F
#endif

GLuint loadTexture(const char * filename,bool useMipMap = true);
void drawAxis(double scale = 1);

#endif //SDLGLUTILS_H
