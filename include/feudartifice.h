#ifndef FEU_H
#define FEU_H

#include "projectile.h"
#include "fusee.h"


class FeuDArtifice : public Form{

  private:
    int nbFusee;
    Fusee* fusees;
    int posX;
    int posY;
    int posZ;
    //propulsion ?

  public:
    FeuDArtifice(); //TODO:default value
    FeuDArtifice(int, Fusee[], int, int, int); //TODO:default value
    void update(double delta_t);
    void render();

};

#endif
