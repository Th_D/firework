#ifndef FUSEE_H
#define FUSEE_H

#include "projectile.h"
#define MAXPROJECT 30

#include "geometry.h"
#include "animation.h"
#include "forms.h"
#include "particule.h"

#include <cmath>
#include <SDL2/SDL_opengl.h>
#include <GL/GLU.h>

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAXB 150
class Fusee : public Projectile, public Form{

  private:
    int nbProjectiles;//fixe
    int nbP = 0;//var
    double tailleProj;
    Particule* projectiles[MAXB]; // la fusee peut lancer des boulettes, mais aussi d'autres fusees = tableau de projectiles
    //TODO:tab a vider quand on kill la fusee
    Form** tabform; // tableau global des elements affichables
    short unsigned int* nbform;
    bool aExplose = false;
    Color color;
    bool enVie;
    double dureeVieProjectiles;
    double vP;

  public:
    Fusee(Form**, short unsigned int*, Color, double, double);
    Fusee(Form** tf, short unsigned int*, double, int, Color, double, Vector, double, double);

    //Fusee(int, Projectile*);
    void update(double delta_t);
    void render();
    bool isAlive();
    void addTempsDeVie(double);


};

#endif
