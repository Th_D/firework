#ifndef PAR_H
#define PAR_H

#include "projectile.h"
#include "geometry.h"
#include "animation.h"
#include "forms.h"

#include <cmath>
#include <SDL2/SDL_opengl.h>
#include <GL/GLU.h>

class Particule : public Projectile, public Form{
  private:
    char* forme;
    bool enVie;
    double taille;

  public:
    Particule(Point, Vector, Color, double, double);
    Particule(Point, Vector, Vector, Color, double);
    ~Particule();

    void kill();
    void update(double delta_t);
    void render();
    bool isAlive();
};

#endif
