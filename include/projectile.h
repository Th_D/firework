#ifndef PROJ_H
#define PROJ_H

#include <stdio.h>
#include <iostream>
#include "geometry.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <math.h>


//TODO:classe abstraite
class Projectile {
private:
    /*double positionX;
    double positionY;
    double positionZ; DANS anim.getPos()*/
    //color couleur;

    /*Vector vit;
    Vector acel;DANS anim.getVit()*/

    double poid;



  protected:
    double dureeDeVie; //fixe


  public:
      double tempsDeVie; //var
    Projectile();
    Projectile(double, double, double, double, double, double, double, double, double, double, double, double);
    ~Projectile();

    void addTempsDeVie(double);
};

void v_boulettes(int, int, Vector, Vector[]);


#endif
