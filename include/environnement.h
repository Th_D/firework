#ifndef ENV_H
#define ENV_H

#include "feudartifice.h"

#define MAXFEU 10

const double g = 3.2;
const double f = 4.1;

class Environnement{
  private:
    double ventX;
    double ventY;
    double ventZ;
    char* imageFond;
    FeuDArtifice* feux;

    //dimensions de la fenetre
    int dimX;
    int dimY;
    int dimZ;

  public:
    Environnement(); // TODO:default values
    Environnement(double, double, double, char*, FeuDArtifice[], int, int, int); //TODO:full args -> default?

};

#endif
