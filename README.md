# Fireworks
### A 3D firework simulation in C++ and OpenGL
Code::Blocks project 

---

A realistic fireworks simulation, with several parameters that can be modified in real time using the keyboard.

Predefined rockets are available with keys 1 to 9.

---

developed for the APP3 end-of-study project 